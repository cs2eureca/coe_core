#include <regex>
#include <itia_futils/itia_futils.h>
#include <coe_core/coe_utilities.h>
#include <coe_core/coe_string_utilities.h>
#include <boost/algorithm/string.hpp>
#include <inttypes.h>
#include <soem/ethercat.h>


namespace coe_core 
{

bool ec_statecheck(uint16 slave, uint16 reqstate, double timeout_s)
{
  uint16 ret = ::ec_statecheck(slave, reqstate,  int(timeout_s * 1e6) );
  if( reqstate != ret )
  {
    ROS_FATAL("Timeout elasped. Abort. Actual state: '%s', Requested %s, ret '%s' "
      , coe_core::to_string((ec_state)(ec_slave[slave].state) ).c_str()
      , coe_core::to_string((ec_state)(reqstate)).c_str()
      , coe_core::to_string((ec_state)(ret)).c_str()
      );
    return false;
  }
  
  return true;
}
      
uint32 soem_check_network_configuration(const uint16_t& NUMBER_OF_SLAVES, const std::map<int, std::string> ordered_list_of_devices)
{
  ROS_INFO("%s%s Check Coherence between yaml and network [%s%s%s]", BOLDYELLOW, ">>>", BOLDMAGENTA, "IN PROGRESS", RESET );
      
      
  /* Do we got expected number of slaves from config */
  if (ec_slavecount < NUMBER_OF_SLAVES)
    return 0;

  for( auto device : ordered_list_of_devices )
  {
    std::regex  allowed_chars_in_name("^[A-Za-z][\w_]*$");
    std::string device_name_from_coe = ec_slave[device.first].name;
    if(!std::regex_match(device_name_from_coe ,allowed_chars_in_name))
    {
      device_name_from_coe  = std::regex_replace(device_name_from_coe , std::regex(R"([^A-Za-z\d])"), "");
    }
    boost::to_lower(device_name_from_coe);

    std::string device_name_from_rosparam = device.second;
    boost::to_lower(device_name_from_rosparam);

    if(device_name_from_coe != device_name_from_rosparam )
    {
      ROS_WARN("Slave %d# '%s' is different from the expected (it was supposed to be '%s'", device.first, device_name_from_coe.c_str(), device_name_from_rosparam.c_str() );
    }
  }
  
  ROS_INFO("%s%s Check Coherence between yaml and network [%s%s%s]", BOLDYELLOW, "<<<", BOLDGREEN, "OK", RESET );

  return 1;
}

bool soem_init(const std::string& adapter_name, const double timeout_s)
{
  ROS_INFO("%s%s Config the SOEM [%s%s%s]", BOLDYELLOW, ">>>", BOLDMAGENTA, "IN PROGRESS", RESET );
  
  if (!ec_init(adapter_name.c_str()) )
  {
    ROS_FATAL("Impossible to init the coe (adapter: '%s') ", adapter_name.c_str() );
    return false;
  }
  
  ROS_INFO("ec_init on %s succeeded.",adapter_name.c_str());
    
  ros::WallTime t0 = ros::WallTime::now();
  while(EcatError) 
  {
    ROS_INFO("%s", ec_elist2string());
    if( ( ros::WallTime::now() - t0 ).toSec() > timeout_s )
    {
      ROS_FATAL("Timeout elasped. Abort " );
      ec_close();
      return false;
    }
    ros::Duration(0.1).sleep();
  }
  
  if ( ec_config_init(FALSE) <= 0 )
  {
    ROS_FATAL("Impossible to config init. No slaves found! Abort." );
    ec_close();
    return false;
  }
  
  ROS_INFO("%s%s Config the SOEM [%s%s%s]", BOLDYELLOW, "<<<", BOLDGREEN, "OK", RESET );

  return true;
}
      
bool soem_init(const std::string& adapter_name, const double timeout_s, const uint16_t& NUMBER_OF_SLAVES, const std::map<int, std::string> ordered_list_of_devices )
{
  
  if ( !soem_init(adapter_name,timeout_s) )
    return false;
  
  ROS_INFO("SOEM Initialized");
  
  if( !soem_check_network_configuration(NUMBER_OF_SLAVES, ordered_list_of_devices ) )
  {
    ROS_FATAL("The network is different form the expected one. Abort.");
    return false;
  }
  ROS_INFO("SOEM Network OK");
  
  return true;
}
      
char* soem_config(const double timeout_s, bool support_dc, bool support_sdoca, const std::map<int,PO2SOconfigFcn>& config_fcn  )
{
  ROS_INFO("%s%s Config the modules [%s%s%s]", BOLDYELLOW, ">>>", BOLDMAGENTA, "IN PROGRESS", RESET );
      
  /** CompleteAccess disabled for Elmo driver */
  ROS_INFO("%s%s%s Support sdo ca? %s", BOLDYELLOW, ">>>", BOLDMAGENTA, ( support_sdoca ? "YES" : "NO" ) );
  if( !support_sdoca )
  {
    for( int slave=1; slave <= ec_slavecount; slave++ )
      ec_slave[slave].CoEdetails ^= ECT_COEDET_SDOCA; 
  }
  
  for( auto const & setup : config_fcn )
  {
    if( setup.second != NULL )
    {
      ec_slave[setup.first].PO2SOconfig = setup.second; 
    }
  }
  
  static char IOmap[4096];
  memset(&IOmap[0],0x0,sizeof(char)* 4096 );
  
  ROS_INFO("Config the IO map" );
  if ( ec_config_map(&IOmap) <= 0 )
  {
    ROS_FATAL("Impossible to get the IOmap" );
    ec_close();
    return NULL;
  }
 
 
  if( support_dc )
  {
    ROS_INFO("Config DC..." );
    if(! ec_configdc() )
    {
      ROS_FATAL("Impossible to config DC. Abort." );
      return NULL;
    }
  }
  
  ROS_INFO("Check errors" );
  ros::WallTime t0 = ros::WallTime::now();
  while(EcatError) 
  {
    ROS_INFO("%s", ec_elist2string());
    if( ( ros::WallTime::now() - t0 ).toSec() > timeout_s )
    {
      ROS_FATAL("Timeout elasped. Abort " );
      ec_close();
      return NULL;
    }
    ros::Duration(0.1).sleep();
  }
  
  ROS_INFO("Command the SAFE_OP to all the slaves" );
  if( !coe_core::ec_statecheck(0, EC_STATE_SAFE_OP,  10.0 ) )
  {
    bool ok = true;
    for (int slave = 1; slave <= ec_slavecount; slave++)
    {
      ok &= coe_core::ec_statecheck(slave, EC_STATE_SAFE_OP,  2.0 );
    }
    if(!ok)
    {
      ec_close();
    }
    return NULL;
  }
  
  ROS_INFO("%s%s Config the modules [%s%s%s]", BOLDYELLOW, "<<<", BOLDGREEN, "OK", RESET );
      
  return IOmap;
}
  
  
bool soem_wait_for_state( const ec_state& target_state )
{
  if( target_state == EC_STATE_OPERATIONAL )
  {
    ec_send_processdata();
    int wkc = ec_receive_processdata(EC_TIMEOUTRET);
    if( wkc <= 0 )
    {
      ROS_FATAL("Error in receiving data ... wkc: %d.", wkc);
      return false;
    }
  }
  
  // 
  if( EC_NOFRAME == ec_writestate(0) )
  {
    ROS_FATAL("Error in writing state to all the slave ...");
    return false;
  }
  
  ec_statecheck(0, target_state,  EC_TIMEOUTSTATE * 3);
  if (ec_slave[0].state != target_state )
  {
    ROS_INFO("Not all slaves reached '%s' state.", coe_core::to_string(target_state).c_str());
    for(int i = 1; i<=ec_slavecount ; i++)
    {
        if(ec_slave[i].state != target_state)
        {
          ROS_INFO("Slave %d State: %s (StatusCode: %4x : %s)",
              i, coe_core::to_string( ec_state( ec_slave[i].state ) ).c_str(), ec_slave[i].ALstatuscode, ec_ALstatuscode2string(ec_slave[i].ALstatuscode));
        }
    }
    return false;
  }
  ec_readstate();
  return true;
}

bool soem_reset_to_operational_state(const double& timeout_t)
{
  bool ret = true;
  ros::WallTime t0 = ros::WallTime::now();
  while( ( ros::WallTime::now() - t0 ).toSec() < timeout_t )
  {
    ec_readstate();
    for (int slave = 1; slave <= ec_slavecount; slave++)
    {
      if ( ec_slave[slave].state != EC_STATE_OPERATIONAL) 
      {
        if (ec_slave[slave].state == (EC_STATE_SAFE_OP + EC_STATE_ERROR))
        {
            ROS_ERROR("Slave %d is in SAFE_OP + ERROR, attempting ack.", slave);
            ec_slave[slave].state = (EC_STATE_SAFE_OP + EC_STATE_ACK);
            ec_writestate(slave);
        }
        else if(ec_slave[slave].state == EC_STATE_SAFE_OP)
        {
            ROS_WARN("Slave %d is in SAFE_OP, change to OPERATIONAL.", slave);
            ec_slave[slave].state = EC_STATE_OPERATIONAL;
            ec_writestate(slave);
        }
        else if(ec_slave[slave].state > EC_STATE_NONE)
        {
            if (ec_reconfig_slave(slave, 500))
            {
              ec_slave[slave].islost = FALSE;
              ROS_INFO("slave %d reconfigured, state %s",slave, coe_core::to_string((ec_state)( ec_slave[slave].state ) ).c_str() );
            }
        }
        else if(!ec_slave[slave].islost)
        {
          /* re-check state */
          ec_statecheck(slave, EC_STATE_OPERATIONAL, EC_TIMEOUTRET);
          if (ec_slave[slave].state == EC_STATE_NONE)
          {
            ec_slave[slave].islost = TRUE;
            printf("ERROR : slave %d lost\n",slave);
          }
        }
      }
      if (ec_slave[slave].islost)
      {
        if(ec_slave[slave].state == EC_STATE_NONE)
        {
            if (ec_recover_slave(slave, 500))
            {
              ec_slave[slave].islost = FALSE;
              printf("MESSAGE : slave %d recovered\n",slave);
            }
        }
        else
        {
            ec_slave[slave].islost = FALSE;
            printf("MESSAGE : slave %d found\n",slave);
        }
      }
      ret &= ( ec_slave[slave].state == EC_STATE_OPERATIONAL );
    }
      
    if( ret )
      return true;
    
    osal_usleep(10000);  
  }

  return false;
}




std::string error_cathed( const char* header ) {
  std::string msg1 =  std::string(header);
 
  throw std::runtime_error("get_information_from_nodes TO DO");
}

int get_node_information  ( const uint16_t cnt ) 
{
 
  /////////////////////////////////////////////////////////////////////////////////////////////
    printf("Slave:%d\n"             , cnt                   );
    printf("Name:%s\n "             , ec_slave[cnt].name    );
    printf("Output size: %dbits\n"  , ec_slave[cnt].Obits   );
    printf("Input  size: %dbits\n"  , ec_slave[cnt].Ibits   );
    printf("State: %d\n"            , ec_slave[cnt].state   );
    printf("Delay: %d[ns]\n"        , ec_slave[cnt].pdelay  );    
    printf("Has DC: %d\n"           , ec_slave[cnt].hasdc   );
    if (ec_slave[cnt].hasdc) 
      printf(" DCParentport:%d\n", ec_slave[cnt].parentport);
    /////////////////////////////////////////////////////////////////////////////////////////////
    
    printf(" Activeports:%d.%d.%d.%d\n", (ec_slave[cnt].activeports & 0x01) > 0 ,
                                  (ec_slave[cnt].activeports & 0x02) > 0 ,
                                  (ec_slave[cnt].activeports & 0x04) > 0 ,
                                  (ec_slave[cnt].activeports & 0x08) > 0 );
    
    printf(" Configured address: %4.4x\n", ec_slave[cnt].configadr);
    
    printf(" Man: %8.8x ID: %8.8x Rev: %8.8x\n", (int)ec_slave[cnt].eep_man, (int)ec_slave[cnt].eep_id, (int)ec_slave[cnt].eep_rev);
    for(int nSM = 0 ; nSM < EC_MAXSM ; nSM++)
    {
        if(ec_slave[cnt].SM[nSM].StartAddr > 0)
          printf(" SM%1d A:%4.4x L:%4d F:%8.8x Type:%d\n",nSM, ec_slave[cnt].SM[nSM].StartAddr, ec_slave[cnt].SM[nSM].SMlength,
                  (int)ec_slave[cnt].SM[nSM].SMflags, ec_slave[cnt].SMtype[nSM]);
    }
    for(int j = 0 ; j < ec_slave[cnt].FMMUunused ; j++)
    {
        printf(" FMMU%1d Ls:%8.8x Ll:%4d Lsb:%d Leb:%d Ps:%4.4x Psb:%d Ty:%2.2x Act:%2.2x\n", j,
                (int)ec_slave[cnt].FMMU[j].LogStart, ec_slave[cnt].FMMU[j].LogLength, ec_slave[cnt].FMMU[j].LogStartbit,
                ec_slave[cnt].FMMU[j].LogEndbit, ec_slave[cnt].FMMU[j].PhysStart, ec_slave[cnt].FMMU[j].PhysStartBit,
                ec_slave[cnt].FMMU[j].FMMUtype, ec_slave[cnt].FMMU[j].FMMUactive);
    }
    printf(" FMMUfunc 0:%d 1:%d 2:%d 3:%d\n",
              ec_slave[cnt].FMMU0func, ec_slave[cnt].FMMU1func, ec_slave[cnt].FMMU2func, ec_slave[cnt].FMMU3func);
    printf(" MBX length wr: %d rd: %d MBX protocols : %2.2x\n", ec_slave[cnt].mbx_l, ec_slave[cnt].mbx_rl, ec_slave[cnt].mbx_proto);
    int ssigen = ec_siifind(cnt, ECT_SII_GENERAL);
    /* SII general section */
    if (ssigen)
    {
        ec_slave[cnt].CoEdetails = ec_siigetbyte(cnt, ssigen + 0x07);
        ec_slave[cnt].FoEdetails = ec_siigetbyte(cnt, ssigen + 0x08);
        ec_slave[cnt].EoEdetails = ec_siigetbyte(cnt, ssigen + 0x09);
        ec_slave[cnt].SoEdetails = ec_siigetbyte(cnt, ssigen + 0x0a);
        if((ec_siigetbyte(cnt, ssigen + 0x0d) & 0x02) > 0)
        {
          ec_slave[cnt].blockLRW = 1;
          ec_slave[0].blockLRW++;
        }
        ec_slave[cnt].Ebuscurrent = ec_siigetbyte(cnt, ssigen + 0x0e);
        ec_slave[cnt].Ebuscurrent += ec_siigetbyte(cnt, ssigen + 0x0f) << 8;
        ec_slave[0].Ebuscurrent += ec_slave[cnt].Ebuscurrent;
    }
    printf(" CoE details: %2.2x FoE details: %2.2x EoE details: %2.2x SoE details: %2.2x\n",
            ec_slave[cnt].CoEdetails, ec_slave[cnt].FoEdetails, ec_slave[cnt].EoEdetails, ec_slave[cnt].SoEdetails);
    printf(" Ebus current: %d[mA]\n only LRD/LWR:%d\n",
            ec_slave[cnt].Ebuscurrent, ec_slave[cnt].blockLRW);

    return 1;
}




/** Read PDO assign structure */
int get_map_pdo_through_sdo ( uint16_t slave, uint16 PDOassign, int mapoffset, int bitoffset )
{
  ec_ODlistt ODlist;
  ec_OElistt OElist;
  uint16 idxloop, nidx, subidxloop, rdat, idx, subidx;
  uint8 subcnt;
  int wkc, bsize = 0, rdl;
  int32 rdat2;
  uint8 bitlen, obj_subidx;
  uint16 obj_idx;
  int abs_offset, abs_bit;

  rdl = sizeof(rdat); rdat = 0;
  
  /* read PDO assign subindex 0 ( = number of PDO's) */
  wkc = ec_SDOread(slave, PDOassign, 0x00, FALSE, &rdl, &rdat, EC_TIMEOUTRXM);
  rdat = etohs(rdat);
  /* positive result from slave ? */
  if ((wkc > 0) && (rdat > 0))
  {
    /* number of available sub indexes */
    nidx = rdat;
    bsize = 0;
    /* read all PDO's */
    for (idxloop = 1; idxloop <= nidx; idxloop++)
    {
      rdl = sizeof(rdat); rdat = 0;
      /* read PDO assign */
      wkc = ec_SDOread(slave, PDOassign, (uint8)idxloop, FALSE, &rdl, &rdat, EC_TIMEOUTRXM);
      /* result is index of PDO */
      idx = etohl(rdat);
      if (idx > 0)
      {
        rdl = sizeof(subcnt); subcnt = 0;
        /* read number of subindexes of PDO */
        wkc = ec_SDOread(slave,idx, 0x00, FALSE, &rdl, &subcnt, EC_TIMEOUTRXM);
        subidx = subcnt;
        /* for each subindex */
        for (subidxloop = 1; subidxloop <= subidx; subidxloop++)
        {
          rdl = sizeof(rdat2); rdat2 = 0;
          /* read SDO that is mapped in PDO */
          wkc = ec_SDOread(slave, idx, (uint8)subidxloop, FALSE, &rdl, &rdat2, EC_TIMEOUTRXM);
          rdat2 = etohl(rdat2);
          /* extract bitlength of SDO */
          bitlen = LO_BYTE(rdat2);
          bsize += bitlen;
          obj_idx = (uint16)(rdat2 >> 16);
          obj_subidx = (uint8)((rdat2 >> 8) & 0x000000ff);
          abs_offset = mapoffset + (bitoffset / 8);
          abs_bit = bitoffset % 8;
          ODlist.Slave = slave;
          ODlist.Index[0] = obj_idx;
          OElist.Entries = 0;
          wkc = 0;
          /* read object entry from dictionary if not a filler (0x0000:0x00) */
          if(obj_idx || obj_subidx)
              wkc = ec_readOEsingle(0, obj_subidx, &ODlist, &OElist);
          printf("  [0x%4.4X.%1d] 0x%4.4X:0x%2.2X 0x%2.2X", abs_offset, abs_bit, obj_idx, obj_subidx, bitlen);
          if((wkc > 0) && OElist.Entries)
          {
              printf(" %-12s %s\n", coe_core::dtype2string(OElist.DataType[obj_subidx]).c_str(), OElist.Name[obj_subidx]);
          }
          else
              printf("\n");
          bitoffset += bitlen;
        };
      };
    };
  };
  /* return total found bitlength (PDO) */
  return bsize;
}





int get_map_pdo_through_sdo(uint16_t slave, char* address_first_io )
{
  static char IOmap[4096];
  memset(IOmap, 0x0, sizeof(char)*4096 );

  int wkc, rdl;
  uint8 nSM;
  int outputs_bo = 0;
  int inputs_bo = 0;
  uint8 SMt_bug_add = 0;

  printf("PDO mapping according to CoE :\n");
  
  rdl = sizeof(nSM); nSM = 0;
  
  /* read SyncManager Communication Type object count */
  wkc = ec_SDOread(slave, ECT_SDO_SMCOMMTYPE, 0x00, FALSE, &rdl, &nSM, EC_TIMEOUTRXM);
  /* positive result from slave ? */
  if ((wkc > 0) && (nSM > 2))
  {
    /* make nSM equal to number of defined SM */
    nSM--;
    /* limit to maximum number of SM defined, if true the slave can't be configured */
    if (nSM > EC_MAXSM)
        nSM = EC_MAXSM;
    /* iterate for every SM type defined */
    for (uint8 iSM = 2 ; iSM <= nSM ; iSM++)
    {
      uint8 tSM;
      rdl = sizeof(tSM); tSM = 0;
      /* read SyncManager Communication Type */
      wkc = ec_SDOread(slave, ECT_SDO_SMCOMMTYPE, iSM + 1, FALSE, &rdl, &tSM, EC_TIMEOUTRXM);
      if (wkc > 0)
      {
        if((iSM == 2) && (tSM == 2)) // SM2 has type 2 == mailbox out, this is a bug in the slave!
        {
          SMt_bug_add = 1; // try to correct, this works if the types are 0 1 2 3 and should be 1 2 3 4
          printf("Activated SM type workaround, possible incorrect mapping.\n");
        }
        if(tSM)
          tSM += SMt_bug_add; // only add if SMt > 0

        if (tSM == 3) // outputs
        {
            /* read the assign RXPDO */
            printf("  SM%1d outputs\n     addr b   index: sub bitl data_type    name\n", iSM);
            int Tsize = get_map_pdo_through_sdo(slave, ECT_SDO_PDOASSIGN + iSM, (int)(ec_slave[slave].outputs - (uint8 *)address_first_io), outputs_bo );
            outputs_bo += Tsize;
        }
        if (tSM == 4) // inputs
        {
            /* read the assign TXPDO */
            printf("  SM%1d inputs\n     addr b   index: sub bitl data_type    name\n", iSM);
            int Tsize = get_map_pdo_through_sdo(slave, ECT_SDO_PDOASSIGN + iSM, (int)(ec_slave[slave].inputs - (uint8 *)address_first_io), inputs_bo );
            inputs_bo += Tsize;
        }
      }
    }
  }

  /* found some I/O bits ? */
  if ((outputs_bo > 0) || (inputs_bo > 0))
    return 1;
  return 0;
}


int get_pdo_through_sii(uint16 slave, uint8 t, int mapoffset, int bitoffset)
{
  uint16 a , w, c, e, er, Size;
  uint8 eectl;
  uint16 obj_idx;
  uint8 obj_subidx;
  uint8 obj_name;
  uint8 obj_datatype;
  uint8 bitlen;
  int totalsize;
  ec_eepromPDOt eepPDO;
  ec_eepromPDOt *PDO;
  int abs_offset, abs_bit;
  char str_name[EC_MAXNAME + 1];

  eectl = ec_slave[slave].eep_pdi;
  Size = 0;
  totalsize = 0;
  PDO = &eepPDO;
  PDO->nPDO = 0;
  PDO->Length = 0;
  PDO->Index[1] = 0;
  for (c = 0 ; c < EC_MAXSM ; c++) PDO->SMbitsize[c] = 0;
  if (t > 1)
      t = 1;
  PDO->Startpos = ec_siifind(slave, ECT_SII_PDO + t);
  if (PDO->Startpos > 0)
  {
      a = PDO->Startpos;
      w = ec_siigetbyte(slave, a++);
      w += (ec_siigetbyte(slave, a++) << 8);
      PDO->Length = w;
      c = 1;
      /* traverse through all PDOs */
      do
      {
          PDO->nPDO++;
          PDO->Index[PDO->nPDO] = ec_siigetbyte(slave, a++);
          PDO->Index[PDO->nPDO] += (ec_siigetbyte(slave, a++) << 8);
          PDO->BitSize[PDO->nPDO] = 0;
          c++;
          /* number of entries in PDO */
          e = ec_siigetbyte(slave, a++);
          PDO->SyncM[PDO->nPDO] = ec_siigetbyte(slave, a++);
          a++;
          obj_name = ec_siigetbyte(slave, a++);
          a += 2;
          c += 2;
          if (PDO->SyncM[PDO->nPDO] < EC_MAXSM) /* active and in range SM? */
          {
              str_name[0] = 0;
              if(obj_name)
                ec_siistring(str_name, slave, obj_name);
              if (t)
                printf("  SM%1d RXPDO 0x%4.4X %s\n", PDO->SyncM[PDO->nPDO], PDO->Index[PDO->nPDO], str_name);
              else
                printf("  SM%1d TXPDO 0x%4.4X %s\n", PDO->SyncM[PDO->nPDO], PDO->Index[PDO->nPDO], str_name);
              printf("     addr b   index: sub bitl data_type    name\n");
              /* read all entries defined in PDO */
              for (er = 1; er <= e; er++)
              {
                  c += 4;
                  obj_idx = ec_siigetbyte(slave, a++);
                  obj_idx += (ec_siigetbyte(slave, a++) << 8);
                  obj_subidx = ec_siigetbyte(slave, a++);
                  obj_name = ec_siigetbyte(slave, a++);
                  obj_datatype = ec_siigetbyte(slave, a++);
                  bitlen = ec_siigetbyte(slave, a++);
                  abs_offset = mapoffset + (bitoffset / 8);
                  abs_bit = bitoffset % 8;

                  PDO->BitSize[PDO->nPDO] += bitlen;
                  a += 2;

                  /* skip entry if filler (0x0000:0x00) */
                  if(obj_idx || obj_subidx)
                  {
                      str_name[0] = 0;
                      if(obj_name)
                        ec_siistring(str_name, slave, obj_name);

                      printf("  [0x%4.4X.%1d] 0x%4.4X:0x%2.2X 0x%2.2X", abs_offset, abs_bit, obj_idx, obj_subidx, bitlen);
                      printf(" %-12s %s\n", coe_core::dtype2string(obj_datatype).c_str(), str_name);
                  }
                  bitoffset += bitlen;
                  totalsize += bitlen;
              }
              PDO->SMbitsize[ PDO->SyncM[PDO->nPDO] ] += PDO->BitSize[PDO->nPDO];
              Size += PDO->BitSize[PDO->nPDO];
              c++;
          }
          else /* PDO deactivated because SM is 0xff or > EC_MAXSM */
          {
              c += 4 * e;
              a += 8 * e;
              c++;
          }
          if (PDO->nPDO >= (EC_MAXEEPDO - 1)) c = PDO->Length; /* limit number of PDO entries in buffer */
      }
      while (c < PDO->Length);
  }
  if (eectl) ec_eeprom2pdi(slave); /* if eeprom control was previously pdi then restore */
  return totalsize;
}


int get_map_pdo_through_sii(uint16_t slave, char* address_first_io )
{
  int retVal = 0;
  int Tsize, outputs_bo, inputs_bo;

  printf("PDO mapping according to SII (Slave Information Interface):\n");

  outputs_bo = 0;
  inputs_bo = 0;
  /* read the assign RXPDOs */
  Tsize = get_pdo_through_sii(slave, 1, (int)(ec_slave[slave].outputs - (uint8*)address_first_io), outputs_bo );
  outputs_bo += Tsize;
  /* read the assign TXPDOs */
  Tsize = get_pdo_through_sii(slave, 0, (int)(ec_slave[slave].inputs - (uint8*)address_first_io), inputs_bo );
  inputs_bo += Tsize;
  /* found some I/O bits ? */
  if ((outputs_bo > 0) || (inputs_bo > 0))
      retVal = 1;
  return retVal;
}



int get_object_description_list (uint16_t cnt, const double timeout_s)
{
  ec_ODlistt ODlist;
  ec_OElistt OElist;

  
  ODlist.Entries = 0;
  memset(&ODlist, 0, sizeof(ODlist));
  if(!ec_readODlist(cnt, &ODlist))
  {
    ros::WallTime t0 = ros::WallTime::now();
    while(EcatError) 
    {
      ROS_INFO("%s", ec_elist2string());
      if( ( ros::WallTime::now() - t0 ).toSec() > timeout_s )
      {
        ROS_FATAL("Timeout elasped. Abort " );
        return -1;
      }
      ros::Duration(0.1).sleep();
    }
    return -1;
  }
      

  printf(" CoE Object Description found, %d entries.\n",ODlist.Entries);
  for( int i = 0 ; i < ODlist.Entries ; i++)
  {
    ec_readODdescription(i, &ODlist);
    
    ros::WallTime t0 = ros::WallTime::now();
    while(EcatError) 
    {
      ROS_INFO("%s", ec_elist2string());
      if( ( ros::WallTime::now() - t0 ).toSec() > timeout_s )
      {
        ROS_FATAL("Timeout elasped. Abort " );
        return -1;
      }
      ros::Duration(0.1).sleep();
    }
            
    printf(">>>>[%d/%d]>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n",i,ODlist.Entries);
    printf("    [Idx:sub] Type Code Length Access Name\t(Value)\n" );
    printf("    [%4.4x:%2.2x]" , ODlist.Index[i], 0x0 );
    printf(" %4.4x "        , ODlist.DataType[i] );
    printf(" %2.2x  "       , ODlist.ObjectCode[i] );
    printf(" %4.4x  "       , 0x0 );
    printf(" %4.4x "        , 0x0 );
    printf(" %s\n"          , ODlist.Name[i] );
    printf("    ------------------------------------------------------------------------------\n");
    memset(&OElist, 0, sizeof(OElist));
    ec_readOE(i, &ODlist, &OElist);

    t0 = ros::WallTime::now();
    while(EcatError) 
    {
      ROS_INFO("%s", ec_elist2string());
      if( ( ros::WallTime::now() - t0 ).toSec() > timeout_s )
      {
        ROS_FATAL("Timeout elasped. Abort " );
        return -1;
      }
      ros::Duration(0.1).sleep();
    }
    
    for( int j = 0 ; j < ODlist.MaxSub[i]+1 ; j++)
    {
      if ((OElist.DataType[j] > 0) && (OElist.BitLength[j] > 0))
      {
        printf("    [%4.4x:%2.2x]", ODlist.Index[i], j  );
        printf(" %4.4x "          , OElist.DataType[j]  );
        printf(" %2.2x  "         , ODlist.ObjectCode[i]);
        printf(" %4.4x  "         , OElist.BitLength[j] );
        printf(" %4.4x "          , OElist.ObjAccess[j] );
        printf(" %s "             ,OElist.Name[j] );
        if ((OElist.ObjAccess[j] & 0x0007))
        {
          printf("(%s)", coe_core::sdo2string(cnt, ODlist.Index[i], j, OElist.DataType[j]).c_str());
        }
        printf("\n");
          
      }
    }
    printf("<<<<[%d/%d]<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n\n",i,ODlist.Entries);
  }
  return 1;
  
}

bool get_cob_via_sdo( uint16_t addr, coe_core::BaseDataObjectEntry* in) 
{
    char usdo[256] = {0};
    int l = in->size();
    int wrc = ec_SDOread(addr, in->index(), in->subindex(), FALSE, &l, &usdo, EC_TIMEOUTRXM);
    std::memcpy( in->data(), usdo,  in->size() );
    return wrc == in->size();
}

bool set_cob_via_sdo( uint16_t addr, const coe_core::BaseDataObjectEntry* in) 
{
    char usdo[256] = {0};
    std::memcpy( &usdo[0], in->data(),in->size() );
    int wrc = ec_SDOwrite(addr, in->index(), in->subindex(), FALSE, in->size(), &usdo[0], EC_TIMEOUTRXM);
    return wrc == in->size();
}


}

