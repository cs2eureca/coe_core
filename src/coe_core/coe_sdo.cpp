
#include <coe_core/coe_sdo.h>

namespace coe_core
{
  
void Sdo::push_back ( const coe_core::DataObject& cdo )
{
    sdo_.push_back ( cdo ); ;
    data_dim_ +=  cdo.nbytes();
    finalized_ = false;
}

const coe_core::DataObject& Sdo::at ( size_t i ) const
{
    if ( !finalized_ )
        throw std::runtime_error ( "The SdoVector has not been finalized. Abort." );
    return sdo_.at ( i );
}
coe_core::DataObject& Sdo::at ( size_t i )
{
    if ( !finalized_ )
        throw std::runtime_error ( "The SdoVector has not been finalized. Abort." );
    return sdo_.at ( i );
}

const coe_core::DataObject& Sdo::operator[] ( size_t i ) const
{
    if ( !finalized_ )
        throw std::runtime_error ( "The SdoVector has not been finalized. Abort." );
    return sdo_[i];
}
coe_core::DataObject& Sdo::operator[] ( size_t i )
{
    if ( !finalized_ )
        throw std::runtime_error ( "The SdoVector has not been finalized. Abort." );
    return sdo_[i];
}

void Sdo::finalize()
{
    finalized_ = true;
}

const uint8_t* Sdo::data ( bool prepend_time ) const
{
    if ( !finalized_ )
        throw std::runtime_error ( "The SdoVector has not been finalized. Abort." );

    static uint8_t* data_buffer_with_time = new uint8_t[ sizeof ( double ) + data_dim_ ]; // time
    static uint8_t* data_buffer_without_time = new uint8_t[ data_dim_ ]; // time

    uint8_t* ptr = prepend_time ? &data_buffer_with_time[0] : &data_buffer_without_time[0];

    if ( prepend_time )
    {
        double act_time = ros::WallTime::now().toSec();
        std::memcpy ( ptr, ( void* ) ( &act_time ), sizeof ( double ) );
        ptr += sizeof ( double );
    }

    for ( size_t i=0; i<sdo_.size(); i++ )
    {
        ptr += sdo_.at ( i ).nbytes();
    }
    return prepend_time ? data_buffer_with_time : data_buffer_without_time;
}


const size_t Sdo::nsdo   ( bool grouped )        const 
{ 
  return grouped 
    ? sdo_.size()
    : std::accumulate( sdo_.begin(), sdo_.end(), 0, []( size_t val, const coe_core::DataObject& p ) { return val + p.size(); }  );
} 
const size_t Sdo::nbytes ( bool prepend_time )   const { return prepend_time ? sizeof(double) + data_dim_ : data_dim_; }

const bool Sdo::operator==( const Sdo& rhs ) const
{
  for( auto jt = rhs.begin(); jt != rhs.end(); jt++ )
  {
    
    auto it = std::find_if( sdo_.begin(), sdo_.end(), [&]( const DataObject& pdoe ) { return (*jt) == pdoe; });
    if( it == sdo_.end() )
    {
      return false;
    }
  }
  
  return  ( data_dim_ == rhs.nbytes(false)    );
}




}

