#ifndef __ITIA_ROS_COE_UTILS__XMLRPC__H__
#define __ITIA_ROS_COE_UTILS__XMLRPC__H__

#include <mutex>
#include <yaml-cpp/yaml.h>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#include <ros/ros.h>
#include <geometry_msgs/Pose.h>
#include <xmlrpcpp/XmlRpc.h>

#include <itia_rutils/itia_rutils_xmlrpc.h>
#include <soem/ethercattype.h>
#include <coe_core/coe_utilities.h>
#include <coe_core/coe_string_utilities.h>
#include <coe_core/coe_pdo.h>
#include <coe_core/coe_sdo.h>

namespace coe_core
{ 

namespace XmlRpcAxisData
{

  static const char* KeysId[5]= { "name"
                                , "scale"
                                , "offset"
                                , "pdo_subindex" };
                                
  enum KeysCode                 { NAME = 0
                                , SCALE
                                , OFFSET
                                , PDO_IDX };
                                
  typedef std::tuple<std::string,double,double,uint8_t> Entry;
                        
  
  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, std::vector<Entry>& entries )
  {
    
    XmlRpc::XmlRpcValue config( node );
    if ( config.getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_ERROR ( "The node  is not of type array. %d/%d", int( config.getType()), int( XmlRpc::XmlRpcValue::TypeArray ) );
      throw std::runtime_error("The node  is not of type array");
    }
    
    entries.clear();
    for(size_t i=0; i < size_t( config.size() ); i++) 
    {
      std::string   name          = itia::rutils::fromXmlRpcValue<std::string>( config[i], KeysId[ NAME    ] , std::to_string(i)+"# name");
      double        scale         = itia::rutils::fromXmlRpcValue<int>        ( config[i], KeysId[ SCALE   ] , std::to_string(i)+"# scale");
      double        offset        = itia::rutils::fromXmlRpcValue<int>        ( config[i], KeysId[ OFFSET  ] , std::to_string(i)+"# offset");
      int           pdo_subindex  = itia::rutils::fromXmlRpcValue<std::string>( config[i], KeysId[ PDO_IDX ] , std::to_string(i)+"# pd subindex");
      
      entries.push_back( std::make_tuple(name, scale, offset, pdo_subindex ) );
    }
  }
    
  inline void toXmlRpcValue( const std::vector<Entry>& entries, XmlRpc::XmlRpcValue& xml_value  )
  {
    const size_t  nentries = entries.size();
    xml_value.setSize( npdo );
    for( auto const & entry : entries )
    {
      itia::rutils::toXmlRpcValue((std::string)std::get< NAME    >(entry)    , xml_value[ pdo_subindex ][ KeysId[ NAME    ] ]  , std::to_string(i)+"# name");
      itia::rutils::toXmlRpcValue((double)     std::get< SCALE   >(entry)    , xml_value[ pdo_subindex ][ KeysId[ SCALE   ] ]  , std::to_string(i)+"# scale");
      itia::rutils::toXmlRpcValue((double)     std::get< OFFSET  >(entry)    , xml_value[ pdo_subindex ][ KeysId[ OFFSET  ] ]  , std::to_string(i)+"# offset");
      itia::rutils::toXmlRpcValue((int)        std::get< PDO_IDX >(entry)    , xml_value[ pdo_subindex ][ KeysId[ PDO_IDX ] ]  , std::to_string(i)+"# pdo subindex");
    }
  }

};

namespace XmlRpcAnalogData
{

  static const char* KeysId[5]= ::coe_core::XmlRpcAxisData::KeysId;
                                
  typedef ::coe_core::XmlRpcAxisData::KeysCode  KeysCode;
                                
  typedef ::coe_core::XmlRpcAxisData::Entry     Entry;
                        
};



namespace XmlRpcDigitalData
{

  static const char* KeysId[5]= { "name"
                                , "pd_subindex"
                                , "bit" };
                                
  enum KeysCode                 { NAME = 0
                                , PDO_IDX 
                                , BIT };
                                
  typedef std::tuple<std::string,uint8_t,uint8_t> Entry;
                        
  
  inline void fromXmlRpcValue( const XmlRpc::XmlRpcValue& node, std::vector<Entry>& entries )
  {
    
    XmlRpc::XmlRpcValue config( node );
    if ( config.getType() != XmlRpc::XmlRpcValue::TypeArray )
    {
      ROS_ERROR ( "The node  is not of type array. %d/%d", int( config.getType()), int( XmlRpc::XmlRpcValue::TypeArray ) );
      throw std::runtime_error("The node  is not of type array");
    }
    
    entries.clear();
    for(size_t i=0; i < size_t( config.size() ); i++) 
    {
      std::string   name          = itia::rutils::fromXmlRpcValue<std::string>( config[i], KeysId[ NAME    ] , std::to_string(i)+"# name");
      int           pdo_subindex  = itia::rutils::fromXmlRpcValue<std::string>( config[i], KeysId[ PDO_IDX ] , std::to_string(i)+"# pd subindex");
      int           bit           = itia::rutils::fromXmlRpcValue<std::string>( config[i], KeysId[ BIT     ] , std::to_string(i)+"# bit");
      
      entries.push_back( std::make_tuple(name, pdo_subindex, bit ) );
    }
  }
    
  inline void toXmlRpcValue( const std::vector<Entry>& entries, XmlRpc::XmlRpcValue& xml_value  )
  {
    const size_t  nentries = entries.size();
    xml_value.setSize( npdo );
    for( auto const & entry : entries )
    {
      itia::rutils::toXmlRpcValue((std::string)std::get< NAME    >(entry)    , xml_value[ pdo_subindex ][ KeysId[ NAME    ] ]  , std::to_string(i)+"# name");
      itia::rutils::toXmlRpcValue((int)        std::get< PDO_IDX >(entry)    , xml_value[ pdo_subindex ][ KeysId[ PDO_IDX ] ]  , std::to_string(i)+"# pdo subindex");
      itia::rutils::toXmlRpcValue((int)        std::get< BIT     >(entry)    , xml_value[ pdo_subindex ][ KeysId[ BIT     ] ]  , std::to_string(i)+"# bit");
    }
  }

};                                              
  

}
#endif





