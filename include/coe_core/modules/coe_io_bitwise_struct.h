/**
 * \file coe_beckhoff_modules_types.h
 * Base structures and functions according the frames exchanged in CANOpen by the bechkoff modules EL3008, EL1809, EL2809, EL2008
 */
#ifndef __coe__bechkoff__modules_types__h__
#define __coe__bechkoff__modules_types__h__

#include <string>
//#include <itia_coe_utils/coe_utilities.h>

namespace coe_core 
{
  
namespace modules 
{

/**
 * @defgroup IO IO Collection of class, functions and structures developed in order to make easy the intefracing with the modules EL3008, EL1809, EL2809, EL2008 of BECKHOFF
 * @ingroup CoE
 */
/**
 * @addtogroup BECKHOFF
 *  @{
 */




typedef struct 
{
    int16_t   value[ 8 ];
}__attribute__ ( ( packed ) ) rpdo_ai8_compact_t ;   // INPUT per il PC


typedef union 
{
    uint16_t   value;
    struct 
    {
        uint8_t ch0 : 1 ;
        uint8_t ch1 : 1 ;
        uint8_t ch2 : 1 ;
        uint8_t ch3 : 1 ;
        uint8_t ch4 : 1 ;
        uint8_t ch5 : 1 ;
        uint8_t ch6 : 1 ;
        uint8_t ch7 : 1 ;
        uint8_t ch8 : 1 ;
        uint8_t ch9 : 1 ;
        uint8_t chA : 1 ;
        uint8_t chB : 1 ;
        uint8_t chC : 1 ;
        uint8_t chD : 1 ;
        uint8_t chE : 1 ;
        uint8_t chF : 1 ;
    } bits;
}__attribute__ ( ( packed ) ) rpdo_di16_compact_t;   // INPUT per il PC

typedef rpdo_di16_compact_t  rpdo_do16_compact_t;    // OUTPUT per il PC

typedef union 
{
    uint8_t   value;
    struct 
    {
        uint8_t ch0 : 1 ;
        uint8_t ch1 : 1 ;
        uint8_t ch2 : 1 ;
        uint8_t ch3 : 1 ;
        uint8_t ch4 : 1 ;
        uint8_t ch5 : 1 ;
        uint8_t ch6 : 1 ;
        uint8_t ch7 : 1 ;
    } bits;
} __attribute__ ( ( packed ) ) rpdo_di18_compact_t;   // INPUT per il PC

typedef rpdo_di8_compact_t  rpdo_do8_compact_t;    // OUTPUT per il PC

void copy( const rpdo_ai8_compact_t& rhs, rpdo_ai8_compact_t* lhs );
void copy( const rpdo_ai8_compact_t* rhs, rpdo_ai8_compact_t* lhs );
rpdo_ai8_compact_t& operator<<(rpdo_ai8_compact_t& lhs, const rpdo_ai8_compact_t& rhs);

void copy( const rpdo_di16_compact_t& rhs, rpdo_di16_compact_t* lhs );
void copy( const rpdo_di16_compact_t* rhs, rpdo_di16_compact_t* lhs );
rpdo_di16_compact_t& operator<<(rpdo_di16_compact_t& lhs, const rpdo_di16_compact_t& rhs);

void copy( const rpdo_di8_compact_t& rhs, rpdo_di8_compact_t* lhs );
void copy( const rpdo_di8_compact_t* rhs, rpdo_di8_compact_t* lhs );
rpdo_di8_compact_t& operator<<(rpdo_di8_compact_t& lhs, const rpdo_di8_compact_t& rhs);

void copy( const el1809_rpdo_t& rhs, el1809_rpdo_t* lhs );
void copy( const el1809_rpdo_t* rhs, el1809_rpdo_t* lhs );
el1809_rpdo_t& operator<<(el1809_rpdo_t& lhs, const el1809_rpdo_t& rhs);

void copy( const el2809_tpdo_t& rhs, el2809_tpdo_t* lhs );
void copy( const el2809_tpdo_t* rhs, el2809_tpdo_t* lhs );
el2809_tpdo_t& operator<<(el2809_tpdo_t& lhs, const el2809_tpdo_t& rhs);


double el3008_digit2volts( int16_t i_val);


}




//--- impl
namespace coe_core 
{


void copy( const rpdo_ai8_compact_t& rhs, rpdo_ai8_compact_t* lhs ) {
    for (int iCh=0;iCh<8;iCh++)
        lhs->value[iCh] = rhs.value[iCh];                        // 0x6041
}
void copy( const rpdo_ai8_compact_t* rhs, rpdo_ai8_compact_t* lhs ) {
    for (int iCh=0;iCh<8;iCh++)
        lhs->value[iCh] = rhs->value[iCh];                        // 0x6041
}
rpdo_ai8_compact_t& operator<<(rpdo_ai8_compact_t& lhs, const rpdo_ai8_compact_t& rhs) {
    copy( rhs, &lhs );
    return lhs;
}
void copy( const rpdo_di16_compact_t& rhs, rpdo_di16_compact_t* lhs ) {
    std::memcpy( lhs, &rhs, sizeof( rpdo_di16_compact_t ));
}
void copy( const rpdo_di16_compact_t* rhs, rpdo_di16_compact_t* lhs ) {
    memcpy( lhs, rhs, sizeof( rpdo_di16_compact_t ));
}
rpdo_di16_compact_t& operator<<(rpdo_di16_compact_t& lhs, const rpdo_di16_compact_t& rhs) {
    memcpy( &lhs, &rhs, sizeof( rpdo_di16_compact_t ));
    return lhs;
}

void copy( const rpdo_di8_compact_t& rhs, rpdo_di8_compact_t* lhs ) {
    memcpy( lhs, &rhs, sizeof( rpdo_di8_compact_t ));
}
void copy( const rpdo_di8_compact_t* rhs, rpdo_di8_compact_t* lhs ) {
    memcpy( lhs, rhs, sizeof( rpdo_di8_compact_t ));
}
rpdo_di8_compact_t& operator<<(rpdo_di8_compact_t& lhs, const rpdo_di8_compact_t& rhs) {
    memcpy( &lhs, &rhs, sizeof( rpdo_di8_compact_t ));
    return lhs;
}


double el3008_digit2volts( int16_t i_val) {

    static const double  d_max_val   =  10.0;
    static const double  d_min_val   = -10.0;
    static const int16_t i_max_val   =  32767;
    static const int16_t i_min_val   = -32768;
    static const double  i_range_val = double(i_max_val) - double(i_min_val);
    double  d_range_val = d_max_val - d_min_val;

    double  perc        = double(i_val - i_min_val) / i_range_val;
    double  ret         = d_min_val + perc * d_range_val;

    return ret;
};


}
}


#endif
